const [buttonModal, buttonScroll] = document.getElementsByClassName("button-large");
const modal = document.getElementsByClassName("modal")[0];
const buttonGo = document.querySelector('.button-small'); // getElementByX() is not the only way to access a DOM element. Also checkout querySelectorAll().


// Modal
buttonModal.addEventListener("click",() => {
    modal.style.display = "flex";
});

modal.addEventListener("click", () => {
    modal.style.display = "none"
});

// Animation
buttonGo.addEventListener('click',() => {
    const item = document.getElementsByTagName("span")[0];
    item.classList.add("position-left");
    setTimeout(() => item.classList.remove("position-left"), 7000);
});

// Scroll
buttonScroll.addEventListener("click", () => {
    window.scroll({
        top: 0,
        behavior: 'smooth' // does not work on Safari
    });
});